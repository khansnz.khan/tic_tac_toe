import React from 'react';
import {View, TextInput, Button, StyleSheet, Modal} from 'react-native';

const RegisterName = ({modalOpen, player1, player2,timer}) => {
  const {player1Name, setPlayer1Name} = player1;
  const {player2Name, setPlayer2Name} = player2;
  const {modalVisible, setModalVisible} = modalOpen;
  const {remainingTime, setRemainingTime}=timer

  const handleStartGame = () => {
    // start the game with player1Name and player2Name
    setModalVisible(false);
    setRemainingTime(60)
  };

  return (
    <View style={styles.container}>
      <Modal visible={modalVisible} animationType="slide">
        <View style={styles.modalContainer}>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder="Player 1 Name"
              value={player1Name}
              onChangeText={text => setPlayer1Name(text)}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder="Player 2 Name"
              value={player2Name}
              onChangeText={text => setPlayer2Name(text)}
            />
          </View>
          <View style={styles.buttonContainer}>
            <Button
              title="Start Game"
              onPress={handleStartGame}
              disabled={!player1Name || !player2Name}
            />
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  inputContainer: {
    marginBottom: 20,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
  },
  buttonContainer: {
    marginTop: 20,
  },
});

export default RegisterName;
