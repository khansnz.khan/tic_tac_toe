import {View, Text, TouchableWithoutFeedback, StyleSheet} from 'react-native';
import React,{useEffect,useState} from 'react';

const Box = ({num, chance, boxInfo, winner,time}) => {
  const {isXChance, setIsXChance} = chance;
  const {boxes, setBoxes} = boxInfo;
  const {remainingTime, setRemainingTime} =time
  const player = isXChance ? 'X' : 'O';

  

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        if (boxes[num] === null && winner === null) {
          setBoxes(prevBoxInfo => {
            prevBoxInfo[num] = player;
            return prevBoxInfo;
          });
          setIsXChance(preState => !preState);
          setRemainingTime(60);
        }
      }}>
      {boxes[num] !== null ? (
        <View style={styles.boxView}>
          {boxes[num] === 'X' ? <Text>X</Text> : <Text>O</Text>}
        </View>
      ) : (
        <View style={styles.boxView}></View>
      )}
    </TouchableWithoutFeedback>
  );
};

export default Box;
const styles = StyleSheet.create({
  boxView: {
    minHeight: 110,
    minWidth: 110,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
  },
});
