import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  Modal,
  TouchableOpacity,
  Image,
} from 'react-native';
import {openDatabase} from 'react-native-sqlite-storage';
var db = openDatabase({name: 'UserDatabase.db'});

const ShowWinner = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [winnersList, setWinnerList] = useState([]);

  useEffect(() => {
    console.log('winnerList', winnersList);
    db.transaction(tx => {
      tx.executeSql('SELECT * FROM Winner', [], (tx, results) => {
        var temp = [];
        for (let i = 0; i < results.rows.length; ++i) {
          console.log('item:', results.rows.item(i));
          temp.push(results.rows.item(i));
        }
        setWinnerList(temp);
      });
    });
  }, [modalVisible]);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => setModalVisible(true)}>
        <Text style={styles.buttonText}>Winner List</Text>
      </TouchableOpacity>
      <Modal
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}>
        <View style={styles.modalContainer}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={styles.list}>
              <Text style={styles.listText}>winner List</Text>
            </View>
            <TouchableOpacity
              style={styles.iconContainer}
              onPress={() => setModalVisible(false)}>
              <Image
                style={styles.closeIcon}
                source={require('./image/close.png')}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={winnersList}
            renderItem={({item}) => (
              <View style={styles.item}>
                <Text style={styles.title}>{item.winnerName}</Text>
              </View>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 50,
  },
  button: {
    backgroundColor: '#2196F3',
    padding: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 50,
    paddingHorizontal: 20,
  },
  list: {
    backgroundColor: '#2196F3',
    padding: 10,
    borderRadius: 5,
    marginBottom: 20,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listText: {
    color: '#fff',
    fontSize: 18,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  title: {
    fontSize: 20,
  },
  iconContainer: {
    // justifyContent:'center',
    alignItems: 'center',
  },
  closeIcon: {
    height: 50,
    width: 50,
  },
});

export default ShowWinner;
