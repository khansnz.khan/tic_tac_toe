import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import Box from './src/Box';
import {openDatabase} from 'react-native-sqlite-storage';
import ShowWinner from './src/ShowWinner';
import RegisterName from './src/RegisterName';
var db = openDatabase({name: 'UserDatabase.db'});

const App = () => {
  const [isXChance, setIsXChance] = useState(true);
  const [boxes, setBoxes] = useState(Array(9).fill(null));
  const [winner, setWinner] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [player1Name, setPlayer1Name] = useState('');
  const [player2Name, setPlayer2Name] = useState('');
  const [remainingTime, setRemainingTime] = useState(60);

  useEffect(() => {
    let timer;
    if (remainingTime > 0 && !winner) {
      timer = setTimeout(() => {
        setRemainingTime(prevTime => prevTime - 1);
      }, 500);
    } else if (remainingTime === 0 && !winner) {
      setIsXChance(prevState => !prevState);
    }
    return () => clearTimeout(timer);
  }, [remainingTime, winner]);

  useEffect(() => {
    setRemainingTime(60);
  }, [isXChance]);

  useEffect(() => {
    setModalVisible(true);
  }, []);

  useEffect(() => {
    db.transaction(txn => {
      txn.executeSql(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='Winner'",
        [],
        (tx, res) => {
          console.log('item:', res.rows.length);
          if (res.rows.length == 0) {
            txn.executeSql('DROP TABLE IF EXISTS Winner', []);
            txn.executeSql(
              'CREATE TABLE IF NOT EXISTS Winner(id INTEGER PRIMARY KEY AUTOINCREMENT, winnerName VARCHAR(20))',
              [],
            );
          }
        },
      );
    });
    if (winner !== null) {
      storeName();
    }
  }, [winner]);

  const PlayBox = num => {
    return (
      <Box
        num={num}
        chance={{isXChance, setIsXChance}}
        boxInfo={{boxes, setBoxes}}
        winner={winner}
        time={{remainingTime, setRemainingTime}}
      />
    );
  };
  const winPosition = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];
  const calculateWin = () => {
    for (let i = 0; i < winPosition.length; i++) {
      if (
        boxes[winPosition[i][0]] !== null &&
        boxes[winPosition[i][0]] === boxes[winPosition[i][1]] &&
        boxes[winPosition[i][0]] === boxes[winPosition[i][2]]
      ) {
        setWinner(boxes[winPosition[i][0]]);
        return;
      }
    }
  };

  useEffect(() => {
    calculateWin();
  }, [isXChance]);

  const resetValue = () => {
    setRemainingTime(60);
    setBoxes(Array(9).fill(null));
    setWinner(null);
    setIsXChance(true);
  };

  const storeName = () => {
    const winnerDatas = winner == 'X' ? player1Name : player2Name;
    db.transaction(function (tx) {
      tx.executeSql(
        'INSERT INTO Winner (winnerName) VALUES (?)',
        [winnerDatas],
        (tx, results) => {
          console.log('Results', results);
          if (results.rowsAffected > 0) {
            Alert.alert('Success', 'New Winner added successfully', [
              {
                text: 'Ok',
                onPress: () => {
                  console.log('ok');
                },
              },
            ]);
          } else {
            Alert.alert('failed');
          }
        },
      );
    });
  };

  return (
    <View style={styles.container}>
      <RegisterName
        modalOpen={{modalVisible, setModalVisible}}
        player1={{player1Name, setPlayer1Name}}
        player2={{player2Name, setPlayer2Name}}
        timer={{remainingTime, setRemainingTime}}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          resetValue();
        }}>
        <Text style={styles.buttonText}>Reset</Text>
      </TouchableOpacity>

      {winner !== null && (
        <Text style={styles.text}>
          {winner == 'X' ? player1Name : player2Name} WON
        </Text>
      )}
      <Text style={[styles.text, {marginVertical: 0}]}>
        Remaining time : {remainingTime}s
      </Text>
      <Text style={styles.text}>
        chance : {isXChance ? player1Name : player2Name}
      </Text>
      <View style={styles.row}>
        {PlayBox(0)}
        {PlayBox(1)}
        {PlayBox(2)}
      </View>
      <View style={styles.row}>
        {PlayBox(3)}
        {PlayBox(4)}
        {PlayBox(5)}
      </View>
      <View style={styles.row}>
        {PlayBox(6)}
        {PlayBox(7)}
        {PlayBox(8)}
      </View>
      <ShowWinner />
    </View>
  );
};

export default App;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  button: {
    marginVertical: 10,
    alignSelf: 'flex-end',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    width: 70,
    padding: 10,
    backgroundColor: 'red',
    marginRight: 40,
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
  },
  text: {
    fontSize: 16,
    fontWeight: '800',
    marginVertical: 20,
  },
});
